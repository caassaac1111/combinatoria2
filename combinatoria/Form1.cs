﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace combinatoria
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private ulong Factorial(int n)
        {
            ulong result = 1;
            for (int i = 2; i <= n; i++)
            {
                result *= (ulong)i;
            }
            return result;
        }

        private ulong Combinatorial(int n, int p)
        {
            return Factorial(n) / (Factorial(p) * Factorial(n - p));
        }

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            if (int.TryParse(textBoxN.Text, out int n) && int.TryParse(textBoxP.Text, out int p))
            {
                ulong result = Combinatorial(n, p);
                labelResult.Text = "El resultado de C(" + n + ", " + p + ") es: " + result;
            }
            else
            {
                MessageBox.Show("Por favor, ingrese valores válidos para n y p.");
            }
        }
    }
}
